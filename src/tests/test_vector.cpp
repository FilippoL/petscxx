#include <iostream>

#define BOOST_TEST_MODULE test_vector test
#include <boost/test/unit_test.hpp>

#include <utilities/math.hpp>

#include <base/context.hpp>

#include <vector/vector.hpp>
#include <vector/vectorexpression.hpp>
#include <vector/vectoriterator.hpp>

#include <io/viewer.hpp>

using namespace petscxx::base;
using namespace petscxx::vector;
using namespace petscxx::io;

BOOST_AUTO_TEST_CASE( test_vector ) {

     Context ctc(boost::unit_test::framework::master_test_suite().argc,
                 boost::unit_test::framework::master_test_suite().argv);

     int size = 101;
     double val = 100.;

     //// Check constructors and some internal state
     Vector u(size), v, w(size, val);
     Vector d = u.duplicate();

     BOOST_CHECK( u.has_type() );

     BOOST_CHECK( u.has_size() );
     BOOST_CHECK( d.has_size() );
     v.set_size(size);
     BOOST_CHECK( v.has_type() );
     BOOST_CHECK( v.has_size() );

     BOOST_CHECK_EQUAL( u.size(), size);
     BOOST_CHECK_EQUAL( d.size(), size);

     // Check move copy and assignment
     Vector cop(u);
     BOOST_CHECK_EQUAL( u.size(), cop.size());
     BOOST_CHECK_EQUAL( u.maxv(), cop.maxv());
     Vector moved = std::move(cop);
     BOOST_CHECK_EQUAL( u.size(), moved.size());
     BOOST_CHECK_EQUAL( u.maxv(), moved.maxv());
     BOOST_CHECK_EQUAL( 0, cop.size());
     BOOST_CHECK_EQUAL( false, cop.is_created());
     Vector moved2(std::move(moved));
     BOOST_CHECK_EQUAL( u.size(), moved2.size());
     BOOST_CHECK_EQUAL( u.maxv(), moved2.maxv());
     BOOST_CHECK_EQUAL( 0, moved.size());
     BOOST_CHECK_EQUAL( false, moved.is_created());

     // Check basic operations: copy, scale, set
     double s = 5.;
     u.set(val); // u = constant val
     v.copy_from(u);
     BOOST_CHECK_EQUAL( v.norm(), u.norm());
     Vector cp;
     cp.set_size(size);
     cp = u;
     BOOST_CHECK_EQUAL( cp.norm(), u.norm());
     Vector cp2;
     u.duplicate();
     // Only duplicate shape, not copy (move of a duplicate)
     cp2 = u.duplicate();
     BOOST_CHECK_NE( cp2.norm(), u.norm());
     // Now we acotually call operator=, calling copy_from
     cp2 = u;
     BOOST_CHECK_EQUAL( cp2.norm(), u.norm());
     v.scale(s); // v = constant s*val
     d.set(val*val); // d = constant val^2
     // w is constant val

     auto dot = u.dot(v);
     BOOST_CHECK_EQUAL( size*(val * val * s), dot);
     BOOST_CHECK_EQUAL( v.dot(u), dot);
     auto dot2 = u.dot(v, w);
     BOOST_CHECK_EQUAL( dot, dot2[0]);
     BOOST_CHECK_EQUAL( size*(val * val), dot2[1]);

     // Test that all arrays are returned correctly
//     u.pre();
//     u.pre_readonly();
//     u.pre();
//     u.pre();
//     u.pre_readonly();
//     u.post();
//     u.pre();
//     u.post();
//     u.post();
//     u.post();
//     u.post();
//     u.post();
//     u.post();
//     check is owned by()

//     std::cout << w.dot<false>(v, v)[1] << std::endl;
//     std::cout << w.dot(v, false) << std::endl;
//     std::cout << w.dot<false>(v, U2)[1] << std::endl;
     BOOST_CHECK(petscxx::utilities::are_almost_equal(
                      w.dot(w),
                      petscxx::utilities::power(w.norm(2)))
                 );

     BOOST_CHECK_EQUAL( val, w.maxv());
     BOOST_CHECK_EQUAL( val*val, d.minv());
     BOOST_CHECK_EQUAL( val, std::get<1>(u.max()));
     BOOST_CHECK_EQUAL( val*s, std::get<1>(v.min()));
//     w.fill([] (int x) { return x; });

//     VectorExpression expr(v), expr1(w), expr2(v);
//     expr = expr1 + expr2;
     v = u + w;
//     std::cout << v;
     BOOST_CHECK_EQUAL( 2*val, std::get<1>(v.max()));
     v = u - w;
     BOOST_CHECK_EQUAL( 0, v.norm());
     v = u - u;
     BOOST_CHECK_EQUAL( 0, v.norm());
     w = w - w + u;
     BOOST_CHECK(petscxx::utilities::are_almost_equal(
                     std::sqrt(val * val * size), w.norm())
                  );
     Vector newb(size);
     newb = w - w + 5 * u * u; // <- wnarrowing
     BOOST_CHECK_CLOSE(5 * std::sqrt(petscxx::utilities::power<4>(val) * size), newb.norm(),
                       0.001 );

     double alpha = 5.;
     w += alpha * u; // call to native axpy
     BOOST_CHECK_EQUAL( val*(alpha+1), w.maxv());
     w += u * alpha; // call to native axpy
     BOOST_CHECK_EQUAL( val*(alpha*2+1), w.minv());
     w += u + u; // cannot call axpy, since u + u is not formed
     BOOST_CHECK_EQUAL( val*(alpha*2+1+2), w.maxv());

     double v_scaling = 7.;
     v.set(v_scaling);
     // u has value val everywhere
     // v has val = 7
     w = u + u + 2.*v; // cannot call axpy, since u + u is not formed
     BOOST_CHECK_EQUAL( size*(val*2 + 2*v_scaling), w.norm(1));


     w += u + u; // cannot call axpy, since u + u is not formed
     BOOST_CHECK_EQUAL( val*4 + 2*v_scaling, w.maxv());
     w += w;
     BOOST_CHECK_EQUAL( 2*(val*4 + 2*v_scaling), w.maxv());
     w += 4* w - u + u;

     double u_val = 8.;
     double v_val = 15.;
     u.set(u_val);
     w.set(v_val);
     BOOST_CHECK_EQUAL( u_val, u.maxv());
     BOOST_CHECK_EQUAL( v_val, w.maxv());
     (u | w | v) = ((w+w) | (u + 5*w) | w);
     BOOST_CHECK_EQUAL( v_val+v_val, u.maxv());
     BOOST_CHECK_EQUAL( v_val+v_val+5*v_val, w.maxv());
     BOOST_CHECK_EQUAL( v_val+v_val+5*v_val, v.maxv());
     using namespace literals;
//     (u, u, v) = (w, u + 5*w, u);
     (1_ph | u | u) = ((w + w) | 1_ph | u);
     // TODO: many vectors => operator, non overloaded?
     // last expr must be a vector?
     BOOST_CHECK_EQUAL( 2*(v_val+v_val+5*v_val), u.minv());
     (1_ph | u) = (u | 1_ph + v);
     BOOST_CHECK_EQUAL( 2*(v_val+v_val+5*v_val) + v.minv(), u.minv());

     w = FunctionExpression<std::function<int(double)>>(
                  [] (int x) { return 5+x; }
               );
     BOOST_CHECK_EQUAL( 5, w.minv());

     std::vector<index_t> ixss = {1,2,3,4,5,6,7};
     std::vector<scalar_t> vals = {1,2,3,4,5,6,7};
     w.set(0, 0., INSERT_VALUES);
     w.set(ixss, vals, INSERT_VALUES);

     auto future = w.assemble();
     std::cout << "Exited assembly." << std::endl;
     future.wait();
     std::cout << "After wait assembly." << std::endl;
     future.get();

//     std::cout << w << std::endl;

     Viewer W(MPI_COMM_WORLD);
     W.open("file.vtk", std::ios_base::out);
     W.write(w);
     W.close();
//     W.open("file", std::ios_base::in);
//     Vector r(size), s2(size), t(size);
//     W.read(r);
//     W.read(t);
//     W.read(s2);
//     W.close();
//     BOOST_CHECK_EQUAL( w.norm(), r.norm());
//     using namespace std::literals::chrono_literals;
//     std::future<void> fut = std::async(std::launch::async, [](){  std::this_thread::sleep_for(10s); });
//     std::cout << "Exited assembly"<< std::flush;
//     fut.wait();
//     std::cout << "Exited assembly"<< std::flush;

     u.set(6);
     int count = 0;
     BOOST_CHECK_EQUAL( u.owned_by_other(), false);
     for(auto it = u.begin(); it < u.end(); ++it) {
         ++count;
         *it = -2;
//         std::cout << *it;
     }
     BOOST_CHECK_EQUAL( count, u.local_size());
     BOOST_CHECK_EQUAL( u.owned_by_other(), false);
     BOOST_CHECK_EQUAL( u.norm(1), 2*size);

     count = 0;
     for(auto& val: u) {
         ++count;
         val = -3;
//         std::cout << it;
     }
     BOOST_CHECK_EQUAL( count, u.local_size());
     BOOST_CHECK_EQUAL( u.owned_by_other(), false);
     BOOST_CHECK_EQUAL( u.norm(1), 3*size);

     count = 0;
     auto fut = u.get_element_wise_access();
     for(int i = u.get_begin(); i < u.get_end(); ++i) {
         ++count;
         u[i] = -4;
//         std::cout << u[i];
     }
     BOOST_CHECK_EQUAL( u.owned_by_other(), true);
     fut.get();
     BOOST_CHECK_EQUAL( count, u.local_size());
     BOOST_CHECK_EQUAL( u.owned_by_other(), false);
     BOOST_CHECK_EQUAL( u.norm(1), 4*size);

     //    126:   VecWAXPY(w,two,x,y);

//    131:   VecPointwiseMult(w,y,x);
//    136:   VecPointwiseDivide(w,x,y);
}
