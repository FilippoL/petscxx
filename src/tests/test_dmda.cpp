#include <iostream>

#define BOOST_TEST_MODULE test_dmda test
#include <boost/test/unit_test.hpp>

#include <utilities/math.hpp>

#include <base/context.hpp>

#include <grid/structuredgrid.hpp>
#include <grid/structuredgridvector.hpp>

using namespace petscxx::base;
using namespace petscxx::vector;
using namespace petscxx::grid;

BOOST_AUTO_TEST_CASE( test_dmda ) {

     Context ctc;
     DMBoundaryType BoundaryNone = DM_BOUNDARY_NONE;

     StructuredGrid<3> s({BoundaryNone, BoundaryNone, BoundaryNone},
            {10,10,10},
            {PETSC_DECIDE,PETSC_DECIDE,PETSC_DECIDE},
            /* Stencil( */
            StructuredGrid<3>::Stencil(1, DMDA_STENCIL_BOX), 2);

     //DMDAStencilType::Box /*)*/);

     //
     //
//     s.set_ownership_range({std::vector<int>{2,8}, std::vector<int>{}, std::vector<int>{}});

     s.print();

     std::vector<std::string> names = {"x", "y"};
     s.set_fields_names(names);

     StructuredGridVector<3> v(s);

     StructuredGridVector<3> g =
             s.get_vector(StructuredGridVector<3>::VectorType::Global);
     StructuredGridVector<3> d =
             s.create_vector(StructuredGridVector<3>::VectorType::Global);

     g.set(5);
     d = g;
     BOOST_CHECK_EQUAL(d.norm(1), g.norm(1));
     g = d+d;
     BOOST_CHECK_EQUAL(2*d.norm(1), g.norm(1));

     g.restore();


}
