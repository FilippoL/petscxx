#pragma once

#include <exception>
#include <string>

namespace petscxx::base {

//!
//! \brief The Exception class is a generic exception thrown by
//! unknown errors within the library. It is meant to be subclassed
//! to provide meaningful error messages and meaningful information.
//!
class Exception : public std::exception {
public:
    Exception(std::string msg = "");
    virtual const char* what() const throw ();

    virtual int error_code() const {
        return error_code_;
    }

private:
    std::string msg;
    static const int error_code_ = -1;
};

class MinValueException : public Exception {
public:
    MinValueException();
};

class WrongStateException : public Exception {
public:
    WrongStateException();
};

class IncompatibleArgumentsException : public Exception {
public:
    IncompatibleArgumentsException();
    virtual int error_code() const {
        return error_code_;
    }
    static const int error_code_ = 75;
};

class NotImplementedException : public Exception {
public:
    NotImplementedException();
};

}
