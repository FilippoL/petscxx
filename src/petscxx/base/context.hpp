#pragma once

#include "mpi.h"

//!
//! \namespace petscxx is the global namespace of the entire library.
//!
//!

//! \namespace base contains all basic routines, from generic utilities to Mat/Vec
//! features.
//!
namespace petscxx::base {

//!
//! \brief The Context class must be instantiated (once) at the beginning
//! of the main loop. This class takes automatically care of initialization
//! of all objects necessary for PETSc/MPI.
//!
//! The class calls PetscInizialize, which in turn initializes MPI. The class
//! also creates static members for error-checking. PETSc/MPI cleanup is done
//! automagically within the destructor.
//!
//! This class is supposed to be a singleton, i.e. instantiated only once,
//! possibly withing main. All members are static.
//!
class Context {
public:
    //!
    //! \brief Context automatically inizialize PETSc and MPI.
    //!
    //!
    //! \param argc Forward arg.-count to PETSc/MPI.
    //! \param argv Forward arg.-values to PETSc/MPI.
    //!
    Context(int argc = 0, char** argv = nullptr);

    //!
    //! \brief Clean up the PETSc and MPI state.
    //!
    //! Calls PetscFinalize, which calls MpiFinalize.
    ~Context();

    //!
    //! \brief SafeCall Wrapper around PETSc error checking CHKERRQ and
    //! CHKERRV, which throws exception on exceptional errors.
    //! \param ierr_ Error code returned by PETSc calls.
    //!
    static void SafeCall(int ierr_);

    //!
    //! \brief worldRank contains the size of the WORLD MPI communicator.
    //! It is just syntactical sugar for the bloated MPI_Comm_rank.
    //!
    static int worldRank;
    //!
    //! \brief worldSize contains the rank within the WORLD MPI communicator.
    //! It is just syntactical sugar for the bloated MPI_Comm_size.
    //!
    static int worldSize;
    //!
    //! \brief World is an alias to MPI_COMM_WORLD
    //!
    static MPI_Comm World;
private:
    //!
    //! \brief ierr contains the value of the last known error, and is used
    //! to meaningfully abnormally terminate the program.
    //!
    static int ierr;
    //!
    //! \brief is_init is true if the singleton Context has been crated.
    //!
    //! Use to free objects at the end of main() and to avoid duplication
    //! of this singleton.
    //!
    static bool is_init;
};

}
