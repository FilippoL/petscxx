#include "exceptions.hpp"

//#include <execinfo.h>

namespace petscxx::base {

Exception::Exception(std::string msg) : msg(msg) {
//    backtrace(buffer, size);
}

const char *Exception::what() const throw() {
    return ("[Error code " + std::to_string(error_code()) + msg).c_str();
}

MinValueException::MinValueException()
    : Exception("Should always be one less then the smallest value.") {

}

WrongStateException::WrongStateException()
    : Exception("Object in argument is in wrong state, e.g. unassembled mat.") {

}

IncompatibleArgumentsException::IncompatibleArgumentsException()
    : Exception("Two arguments are incompatible.") {

}

NotImplementedException::NotImplementedException()
    : Exception("Feature not implemented.") {

}

}
