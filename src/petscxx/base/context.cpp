#include "context.hpp"

#include "exceptions.hpp"

#include "petscsys.h"

namespace petscxx::base {

int Context::ierr = 0;
bool Context::is_init = false;
int Context::worldRank = -1;
int Context::worldSize = -1;
MPI_Comm Context::World = 0;

Context::Context(int argc, char** argv) {
    if(is_init) return;
    ierr = PetscInitialize(&argc, &argv, (char*)0, nullptr);
    World = PETSC_COMM_WORLD;
    MPI_Comm_rank(World, &worldRank);
    MPI_Comm_size(World, &worldSize);
    is_init = true;
}

Context::~Context() {
    if(is_init) {
        ierr = PetscFinalize();
        is_init = false;
    }
}

void Context::SafeCall(int ierr_) {
//    CHKERRV(ierr_);
    switch(ierr_) {
        case 0:
            return;
        default:
            throw Exception("Unknown PETSc error code.");
        case PETSC_ERR_MIN_VALUE:
            throw MinValueException();
        case PETSC_ERR_MEM: // 55
            throw Exception();
        /* unable to allocate requested memory */
        case PETSC_ERR_SUP: // 56
            throw Exception();
        /* no support for requested operation */
        case PETSC_ERR_SUP_SYS: // 57
            throw Exception();
        /* no support for requested operation on this computer system */
        case PETSC_ERR_ORDER: // 58
            throw Exception();
       /* operation done in wrong order */
        case PETSC_ERR_SIG: // 59
            throw Exception();
       /* signal received */
        case PETSC_ERR_FP: // 72
            throw Exception();
        /* floating point exception */
        case PETSC_ERR_COR: // 74
            throw Exception();
        /* corrupted PETSc object */
        case PETSC_ERR_LIB: // 76
            throw Exception();
        /* error in library called by PETSc */
        case PETSC_ERR_PLIB: // 77
            throw Exception();
        /* PETSc library generated inconsistent data */
        case PETSC_ERR_MEMC: // 78
            throw Exception();
        /* memory corruption */
        case PETSC_ERR_CONV_FAILED: // 82
            throw Exception();
        /* iterative method (KSP or SNES) failed */
        case PETSC_ERR_USER: // 83
            throw Exception();
        /* user has not provided needed function */
        case PETSC_ERR_SYS: // 88
            throw Exception();
        /* error in system call */
        case PETSC_ERR_POINTER: // 70
            throw Exception();
        /* pointer does not point to valid address */
        case PETSC_ERR_ARG_SIZ: // 60
            throw Exception();
        /* nonconforming object sizes used in operation */
        case PETSC_ERR_ARG_IDN: // 61
            throw Exception();
        /* two arguments not allowed to be the same */
        case PETSC_ERR_ARG_WRONG: // 62
            throw Exception();
        /* wrong argument (but object probably ok) */
        case PETSC_ERR_ARG_CORRUPT: // 64
            throw Exception();
        /* null or corrupted PETSc object as argument */
        case PETSC_ERR_ARG_OUTOFRANGE: // 63
            throw Exception();
        /* input argument, out of range */
        case PETSC_ERR_ARG_BADPTR: // 68
            throw Exception();
        /* invalid pointer argument */
        case PETSC_ERR_ARG_NOTSAMETYPE: // 69
            throw Exception();
        /* two args must be same object type */
        case PETSC_ERR_ARG_NOTSAMECOMM: // 80
            throw Exception();
        /* two args must be same communicators */
        case PETSC_ERR_ARG_WRONGSTATE: // 73
            throw WrongStateException();
        case PETSC_ERR_ARG_TYPENOTSET: // 89
            throw Exception();
        /* the type of the object has not yet been set */
        case PETSC_ERR_ARG_INCOMP: // 75
            throw IncompatibleArgumentsException();
        case PETSC_ERR_ARG_NULL: // 85
            throw Exception();
        /* argument is null that should not be */
        case PETSC_ERR_ARG_UNKNOWN_TYPE: // 86
            throw Exception();
        /* type name doesn't match any registered type */
        case PETSC_ERR_FILE_OPEN: // 65
            throw Exception();
        /* unable to open file */
        case PETSC_ERR_FILE_READ: // 66
            throw Exception();
        /* unable to read from file */
        case PETSC_ERR_FILE_WRITE: // 67
            throw Exception();
        /* unable to write to file */
        case PETSC_ERR_FILE_UNEXPECTED: // 79
            throw Exception();
        /* unexpected data in file */
        case PETSC_ERR_MAT_LU_ZRPVT: // 71
            throw Exception();
        /* detected a zero pivot during LU factorization */
        case PETSC_ERR_MAT_CH_ZRPVT: // 81
            throw Exception();
        /* detected a zero pivot during Cholesky factorization */
        case PETSC_ERR_INT_OVERFLOW: // 84
            throw Exception();
        case PETSC_ERR_FLOP_COUNT: // 90
            throw Exception();
        case PETSC_ERR_NOT_CONVERGED: // 91
            throw Exception();
        /* solver did not converge */
        case PETSC_ERR_MISSING_FACTOR: // 92
           throw Exception();
        /* MatGetFactor() failed */
        case PETSC_ERR_OPT_OVERWRITE: // 93
           throw Exception();
        /* attempted to over wrote options which should not be changed */
        case PETSC_ERR_MAX_VALUE: // 94
            throw Exception();
            /* this is always the one more than the largest error code */
    }
}

}
