#pragma once

#include <smmintrin.h>
#include <immintrin.h>

#include <string>

#if defined CC_GNU || defined CC_CLANG
#    define UNLIKELY(cond)     __builtin_expect(!!(cond),0)
#    define LIKELY(cond)       __builtin_expect(!!(cond),1)
#else
#    define UNLIKELY(cond)     (cond)
#    define LIKELY(cond)       (cond)
#endif

#if defined CC_MSVC || defined CC_INTEL
#    define ALWAYS_INLINE     __forceinline
#elif defined CC_GNU || defined CC_CLANG
#    define ALWAYS_INLINE     __attribute__((always_inline)) inline
#else
#    define ALWAYS_INLINE     inline
#endif

#if defined CC_GNU || defined CC_CLANG
#    define NEVER_INLINE      __attribute__((noinline))
#elif CC_MSVC
#    define NEVER_INLINE      __declspec(noinline)
#else
#    define NEVER_INLINE
#endif

#ifndef UNUSED
#    define UNUSED(x)        (void)x
#endif

template <typename T>
constexpr unsigned int misalignment_of(T* ptr,
                                    unsigned int byte_count) {
    return reinterpret_cast<std::uintptr_t>(ptr) % byte_count;
}

//!
//!
template <typename T>
constexpr bool is_aligned(T* ptr,
                          unsigned int byte_count) {
    return misalignment_of(ptr, byte_count) == 0;
}

inline void realign(double **x_, int bytes = 16) {
    if(is_aligned(*x_, bytes)) return;
    *reinterpret_cast<char**>(x_) +=
            bytes - misalignment_of(*x_, bytes);
    assert(is_aligned(*x_, bytes)
           && "Must be aligned to " + std::to_stirng(bytes) + "!");
}

