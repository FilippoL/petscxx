#pragma once

#include <smmintrin.h>
#include <immintrin.h>

#include "c17_backports.hpp"

namespace petscxx::simd {

template<unsigned int N, typename T>
using vector = std::conditional_t<N == 2,
                   std::conditional_t<is_same_v<T, double>,
                        __m128d, void>,
               std::conditional_t<N == 4,
                   std::conditional_t<is_same_v<T, double>,
                        __m256d , void>,
                   void>> __attribute__ ((__vector_size__ (N*sizeof(double)), __may_alias__));

}
