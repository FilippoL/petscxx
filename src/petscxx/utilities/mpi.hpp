#pragma once

namespace petscxx::utilities {

inline int get_size(MPI_Comm comm) {
    int size;
    MPI_Comm_size(comm, &size);
    return size;
}

}
