#pragma once

namespace petscxx::utilities {

template <class T, unsigned long int d>
std::ostream & operator<<(std::ostream & o, const std::array<T, d> & container) {
    o << "[";
    auto it = std::begin(container);
    for(auto& elem: container) {
        o << elem;
        if(++it != std::end(container)) o << ", ";
    }
    return  o << "]";
}

}
