#pragma once
//!
//! \brief is_same_v intruduces the syntactical sugar for
//! type equality, first introduced in C++17 but not implemented
//! on many compilers.
//!
//! Usage:
//!     is_same_v<type1, type2> is true if type1 is the same as type2
//!
//! \tparam T fisrt type for comparison
//! \tparam V second type for comparison
template< class T, class U >
constexpr bool is_same_v = std::is_same<T, U>::value;

//!
//! \brief pow recursive constexpr implementation
//! of integer power function.
//!
//! Can be evaluated at compile time. The runtime
//! implementation is not the most efficient.
//! \tparam T type of base
//! \param base Value of base
//! \param exp Integer exponent
//! \return base^exp with \f$exp \in \mathbf{N}\f$
//!
template <typename T>
constexpr T pow(T base, int exp) {
    return (exp > 0) ? base * pow(base, exp-1) : 1;
}

template <char...>
struct literal;

//!
//! \brief literal A specialization of the literal class
//! for an empty parameter-pack. The value defaults to zero.
//!
template <>
struct literal<> {
    //! Specialization for recursion termination on empy pack.
    static const unsigned int to_int = 0;
};

//!
//! \brief specialization of literal for a non-empty
//! parameter pack of chars, e.g. for literal<'b','c','d'>
//!
//! This structure makes sense only if all template arguments
//! are char digits.
//!
template <char c, char ...cv> struct literal<c, cv...> {
    static constexpr char char_zero = '0';
    static_assert(c - char_zero >= 0 && c - char_zero <= 9,
                  "Character must be a digit.");
    //! Integer representation of character pack as base-10 expansion.
    static const unsigned int to_int =
            (c - char_zero) * pow(10, sizeof...(cv)) + literal<cv...>::to_int;
};
