#pragma once

#include "petscsys.h"

namespace petscxx::base {

using real_t = PetscReal;
using scalar_t = PetscScalar;
using index_t = PetscInt;

}
