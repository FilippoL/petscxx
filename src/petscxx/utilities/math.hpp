#pragma once

#include <cmath>
#include <limits>

namespace petscxx::utilities {

namespace internal {
class _pow {
public:
    template <unsigned int N, class T>
    constexpr inline static double pow(const T & x,
                      typename std::enable_if<N == 1,
                             bool>::type = false
                      ) {
        // Identity
        return x;
    }

    template <unsigned int N, class T>
    constexpr inline static double pow(const T & x,
                      typename std::enable_if<N != 0 && (N % 2 == 0),
                             bool>::type
                      = false) {
        // $x^N = x^{N/2}x^{N/2}$
        double y = pow<N/2>(x);
        return y*y;
    }

    template <unsigned int N, class T>
    constexpr inline static double pow(const T & x,
                      typename std::enable_if<N != 1 && (N % 2 != 0),
                             bool>::type
                      = false) {
        // $x^N = x^{N/2-1}x^{N/2-1}x$
        double y = pow<(N-1)/2>(x);
        return x*y*y;
    }
};
}

template <unsigned int N = 2, class T>
constexpr inline T power(const T & val) {
    return internal::_pow::pow<N>(val);
}

template <class T, class U>
bool are_almost_equal(const T & a, const U & b) {
    return std::abs(a - b) < std::numeric_limits<T>::epsilon() * std::fabs(a);
}

}
