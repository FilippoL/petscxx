#pragma once

#include <type_traits>
#include <iostream>

#include "petscvec.h"

#include "utilities/types.hpp"
#include "utilities/alignment.hpp"
#include "utilities/simd.hpp"
#include "utilities/c17_backports.hpp"

namespace petscxx::vector {

using namespace petscxx::base;
using namespace petscxx;

//////
///
///  Abstract expressions
///
//////
template <class Expr>
class Expression {
public:
    ALWAYS_INLINE double operator[](index_t i) const {
        return static_cast<const Expr&>(*this)[i];
    }

    ALWAYS_INLINE Expr& operator()() {
        return static_cast<Expr&>(*this);
    }
    ALWAYS_INLINE const Expr& operator()() const {
        return static_cast<const Expr&>(*this);
    }

    ALWAYS_INLINE __m256d evalat(int i) const {
        return static_cast<const Expr&>(*this).evalat(i);
    }
    using is_expression = bool;
};

template <class Function>
class FunctionExpression
        : public Expression<FunctionExpression<Function>> {
public:
    const Function & func;

    FunctionExpression(const Function & func)
        : func(func) {
    }

    void pre_readonly() const { }
    void pre() const { }
    void post() const { }

    ALWAYS_INLINE scalar_t operator[](index_t i) const {
        return func(i);
    }

    ALWAYS_INLINE __m256d evalat(int i) const {
        __m256d ret = {
            static_cast<scalar_t>(func(i)),
            static_cast<scalar_t>(func(i+1)),
            static_cast<scalar_t>(func(i+2)),
            static_cast<scalar_t>(func(i+3))
        };
        return ret;
    }
};

template <template<class, class> class BinaryOp, class ExprLeft, class ExprRight>
class BinaryExpression : public Expression<BinaryOp<ExprLeft, ExprRight>> {
public:
    const ExprLeft & left_expr_;
    const ExprRight & right_expr_;

    BinaryExpression(const ExprLeft & u, const ExprRight & v)
        : left_expr_(u), right_expr_(v) {
    }

    void pre_readonly() const { left_expr_().pre_readonly(); right_expr_().pre_readonly(); }
    void post() const { left_expr_().post(); right_expr_().post(); }
};

template <unsigned int N>
class TemporaryVariable : public Expression<TemporaryVariable<N>> {
public:
    static __m256d content;
    static scalar_t scalar_content;

    TemporaryVariable() {
    }

    ALWAYS_INLINE scalar_t& operator[](index_t) {
        return scalar_content;
    }

    ALWAYS_INLINE scalar_t operator[](index_t) const {
        return scalar_content;
    }

    ALWAYS_INLINE __m256d& evalat(int) const {
        return content;
    }

    void pre() const { }
    void pre_readonly() const { }
    void post() const { }

    template <char... lit>
    friend constexpr decltype(auto) operator "" _ph();
};

template <unsigned int N>
__m256d TemporaryVariable<N>::content;

template <unsigned int N>
scalar_t TemporaryVariable<N>::scalar_content;

namespace literals {

template <char... lit>
constexpr decltype(auto) operator"" _ph() {
    return TemporaryVariable<literal<lit...>::to_int>();
}

} // end namespace literals

//////
///
///  Vector/Vector expression
///
//////
template <class ExprLeft, class ExprRight>
class VectorSum
        : public Expression<VectorSum<ExprLeft, ExprRight>> {
    const ExprLeft & left_expr_;
    const ExprRight & right_expr_;
public:
    VectorSum(const ExprLeft & u, const ExprRight & v)
        : left_expr_(u), right_expr_(v) {
    }

    void pre_readonly() const { left_expr_.pre_readonly(); right_expr_.pre_readonly(); }
    void post() const { left_expr_.post(); right_expr_.post(); }

    ALWAYS_INLINE double operator[](index_t i) const {
        return left_expr_[i] + right_expr_[i];
    }

    ALWAYS_INLINE __m256d evalat(int i) const {
        return this->left_expr_.evalat(i) + this->right_expr_.evalat(i);
    }
};

template <class ExprLeft, class ExprRight>
class VectorDiff
        : public Expression<VectorDiff<ExprLeft, ExprRight> > {
    const ExprLeft & left_expr_;
    const ExprRight & right_expr_;
public:
    VectorDiff(const ExprLeft & u, const ExprRight & v)
        : left_expr_(u), right_expr_(v) {
    }

    void pre_readonly() const { left_expr_().pre_readonly(); right_expr_().pre_readonly(); }
    void post() const { left_expr_().post(); right_expr_().post(); }

    ALWAYS_INLINE double operator[](index_t i) const {
        return left_expr_[i] - right_expr_[i];
    }

    ALWAYS_INLINE __m256d evalat(int i) const {
        return this->left_expr_.evalat(i) - this->right_expr_.evalat(i);
    }
};

template <class ExprLeft, class ExprRight>
class VectorProd
        : public BinaryExpression<VectorProd, ExprLeft, ExprRight> {
public:
    VectorProd(const ExprLeft & left_expr_, const ExprRight & right_expr_)
    // FUCK YOU CLANG :)
        : BinaryExpression<::petscxx::vector::VectorProd, ExprLeft, ExprRight>(left_expr_, right_expr_) {}

    ALWAYS_INLINE double operator[](index_t i) const {
        return this->left_expr_[i] * this->right_expr_[i];
    }

    ALWAYS_INLINE __m256d evalat(int i) const {
        return this->left_expr_.evalat(i) * this->right_expr_.evalat(i);
    }
};

template <class ExprLeft, class ExprRight>
class VectorDiv
        : public BinaryExpression<::petscxx::vector::VectorDiv, ExprLeft, ExprRight> {
public:
    VectorDiv(const ExprLeft & left_expr_, const ExprRight & right_expr_)
        : BinaryExpression<VectorProd, ExprLeft, ExprRight>(left_expr_, right_expr_) {}

    ALWAYS_INLINE double operator[](index_t i) const {
        return this->left_expr_[i] / this->right_expr_[i];
    }

    ALWAYS_INLINE __m256d evalat(int i) const {
        return this->left_expr_.evalat(i) / this->right_expr_.evalat(i);
    }
};

//////
///
///  Scalar/Vector expressions
///
//////
template <class ScalarLeft, class ExprRight>
class VectorScaling
        : public Expression<VectorScaling<ScalarLeft, ExprRight> > {
public:
    const ScalarLeft & left_expr_;
    const ExprRight & right_expr_;
    const simd::vector<4, double> packet_;
    static_assert(std::is_scalar<ScalarLeft>(),
                  "Left operand of binary scalar/vector must be a scalar.");
    VectorScaling(const ScalarLeft & u, const ExprRight & v)
        : left_expr_(u), right_expr_(v)
        , packet_{static_cast<scalar_t>(this->left_expr_),
                  static_cast<scalar_t>(this->left_expr_),
                  static_cast<scalar_t>(this->left_expr_),
                  static_cast<scalar_t>(this->left_expr_)} {

    }

    void pre_readonly() const { right_expr_().pre_readonly(); }
    void post() const { right_expr_().post(); }

    ALWAYS_INLINE double operator[](index_t i) const {
        return this->left_expr_ * this->right_expr_[i];
    }

    ALWAYS_INLINE __m256d evalat(int i) const {
//        __m256d scalar = {this->left_expr_, this->left_expr_,
//                          this->left_expr_, this->left_expr_};
        return packet_ * this->right_expr_.evalat(i);
    }
};

template <class ScalarLeft, class ExprRight>
class VectorInvert
        : public Expression<VectorInvert<ScalarLeft, ExprRight> > {
public:
    const ScalarLeft & left_expr_;
    const ExprRight & right_expr_;
    static_assert(std::is_scalar<ScalarLeft>(),
                  "Left operand of binary scalar/vector must be a scalar.");
    VectorInvert(const ScalarLeft & u, const ExprRight & v)
        : left_expr_(u), right_expr_(v) {
    }

    void pre_readonly() const { right_expr_().pre_readonly(); }
    void post() const { right_expr_().post(); }

    ALWAYS_INLINE double operator[](index_t i) const {
        return this->left_expr_ / this->right_expr_[i];
    }

    ALWAYS_INLINE __m256d evalat(int i) const {
        __m256d scalar = {this->left_expr_, this->left_expr_,
                          this->left_expr_, this->left_expr_};
        return scalar / this->right_expr_.evalat(i);
    }
};


template <class ExprLeft, class ExprRight>
class VectorZip
        : public Expression<VectorZip<ExprLeft, ExprRight>> {
public:
    ExprLeft & left_expr_;
    ExprRight & right_expr_;

    void pre() const { left_expr_().pre(); right_expr_().pre(); }
    void pre_readonly() const { left_expr_().pre_readonly(); right_expr_().pre_readonly(); }
    void post() const { left_expr_().post(); right_expr_().post(); }


//    template <typename ExprLeft, typename ExprRight,
//              typename T = typename std::remove_reference_t<ExprLeft>::is_expression>

    VectorZip(ExprLeft & left_expr_, ExprRight & right_expr_)
        : left_expr_(left_expr_), right_expr_(right_expr_) {}

    template<unsigned int N>
    void get() {}

//    template<>
    const ExprLeft & get0() const { return left_expr_; }
//    template<>
    const ExprRight & get1() const { return right_expr_; }

//    VectorZip & operator=(const VectorZip&) = delete;
//    VectorZip(const VectorZip&) = delete;
//    VectorZip() = delete;

//    template <class ExprLeftRhs, class ExprRightRhs>
//    inline void operator=(const VectorZip<ExprLeftRhs, ExprRightRhs> & expr) {
////        assert(has_size() && "Assigned vector has no size!");
//        index_t nlocal;
//        VecGetLocalSize(right_expr_.get_vec(), &nlocal);
//        left_expr_.pre();
//        right_expr_.pre();
//        expr().pre_readonly();

////        if(!is_aligned(left_expr_.get_array(), 32)) std::cout << "Misalignment!";
////        if(!is_aligned(right_expr_.get_array(), 32)) std::cout << "Misalignment!";

//        for (index_t i = 0; i < nlocal; i+=4) {
//            *((__m256d*) (left_expr_.get_array() + i)) = expr().get0().evalat(i);
//            *((__m256d*) (right_expr_.get_array() + i)) = expr().get1().evalat(i);
//        }
//        left_expr_.post();
//        right_expr_.post();
//        expr().post();
////        vec_array = nullptr;

////        return *this;
//    }

    template <class ExprLeftRhs, class ExprRightRhs>
    ALWAYS_INLINE void unroll(const VectorZip<ExprLeftRhs, ExprRightRhs> & expr, index_t i) {
//        *((__m256d*) (left_expr_.get_array() + i)) = expr().get0().evalat(i);
//        *((__m256d*) (right_expr_.get_array() + i)) = expr().get1().evalat(i);
          left_expr_.evalat(i) = expr().get0().evalat(i);
          right_expr_.evalat(i) = expr().get1().evalat(i);
    }

    template <class ExprLeft0Rhs, class ExprLeft1Rhs, class ExprRightRhs>
    ALWAYS_INLINE void unroll(const VectorZip<
                                        VectorZip<ExprLeft0Rhs, ExprLeft1Rhs>,
                                            ExprRightRhs> & expr, index_t i) {
        left_expr_.unroll(expr.left_expr_, i);
        right_expr_.evalat(i) = expr().get1().evalat(i);
//        *((__m256d*) (right_expr_.get_array() + i)) = expr().get1().evalat(i);
    }

    template <class ExprLeftRhs, class ExprRightRhs>
    ALWAYS_INLINE void unroll_single(const VectorZip<ExprLeftRhs, ExprRightRhs> & expr, index_t i) {
//        *((__m256d*) (left_expr_.get_array() + i)) = expr().get0().evalat(i);
//        *((__m256d*) (right_expr_.get_array() + i)) = expr().get1().evalat(i);
          left_expr_[i] = expr().get0()[i];
          right_expr_[i] = expr().get1()[i];
    }

    template <class ExprLeft0Rhs, class ExprLeft1Rhs, class ExprRightRhs>
    ALWAYS_INLINE void unroll_single(const VectorZip<
                                        VectorZip<ExprLeft0Rhs, ExprLeft1Rhs>,
                                            ExprRightRhs> & expr, index_t i) {
        left_expr_.unroll_single(expr.left_expr_, i);
        right_expr_[i] = expr().get1()[i];
//        *((__m256d*) (right_expr_.get_array() + i)) = expr().get1().evalat(i);
    }


    template <class ExprLeftRhs, class ExprRightRhs>
    inline void operator=(const VectorZip<ExprLeftRhs, ExprRightRhs> & expr) {
//        assert(has_size() && "Assigned vector has no size!");
        index_t nlocal;
        VecGetLocalSize(right_expr_.get_vec(), &nlocal);
        left_expr_.pre();
        right_expr_.pre();
        expr().pre_readonly();

//        if(!is_aligned(left_expr_.get_array(), 32)) std::cout << "Misalignment!";
//        if(!is_aligned(right_expr_.get_array(), 32)) std::cout << "Misalignment!";

        // TODO: move packet size
        constexpr int packetSize = 4;
        for (index_t i = 0; i+packetSize < nlocal; i+=packetSize) {
            unroll(expr, i);
        }
        // TODO:peeling
        int residual = nlocal % packetSize;
        while(residual > 0) {
            unroll_single(expr, nlocal - residual);
            --residual;
        }

        left_expr_.post();
        right_expr_.post();
        expr().post();
//        vec_array = nullptr;

//        return *this;
    }
};

//////
///
///  Vector/Vector operations
///
//////
template <typename ExprLeft, typename ExprRight>
const VectorSum<ExprLeft, ExprRight>
ALWAYS_INLINE operator+(ExprLeft const& u, ExprRight const& v) {
    return VectorSum<ExprLeft, ExprRight>(u, v);
}

template <typename ExprLeft, typename ExprRight>
const VectorDiff<ExprLeft, ExprRight>
ALWAYS_INLINE operator-(const ExprLeft & u, const ExprRight& v) {
    return VectorDiff<ExprLeft, ExprRight>(u, v);
}

template <typename ExprLeft, typename ExprRight>
const VectorProd<Expression<ExprLeft>, Expression<ExprRight>>
ALWAYS_INLINE operator*(Expression<ExprLeft> const& u, Expression<ExprRight> const& v) {
    return VectorProd<Expression<ExprLeft>, Expression<ExprRight>>(u, v);
}

template <typename ExprLeft, typename ExprRight>
const VectorDiv<Expression<ExprLeft>, Expression<ExprRight>>
ALWAYS_INLINE operator/(Expression<ExprLeft> const& u, Expression<ExprRight> const& v) {
    return VectorDiv<Expression<ExprLeft>, Expression<ExprRight>>(u, v);
}

//////
///
///  Scalar/Vector operations
///
//////
template <typename T, typename ExprRight,
          typename std::enable_if<std::is_scalar<T>::value, bool>::type scalar = true>
const VectorScaling<T, Expression<ExprRight>>
ALWAYS_INLINE operator*(const T& scaling, const Expression<ExprRight> & v) {
    return VectorScaling<T, Expression<ExprRight>>(scaling, v);
}

template <typename T, typename ExprLeft,
          typename std::enable_if<std::is_scalar<T>::value, bool>::type scalar = true>
const VectorScaling<T, Expression<ExprLeft>>
ALWAYS_INLINE operator*(const Expression<ExprLeft> & v, const T& scaling) {
    return VectorScaling<T, Expression<ExprLeft>>(scaling, v);
}

template <typename T, typename ExprRight,
          typename std::enable_if<std::is_scalar<T>::value, bool>::type scalar = true>
const VectorScaling<T, Expression<ExprRight>>
ALWAYS_INLINE operator/(const T& scaling, const Expression<ExprRight> & v) {
    return VectorInvert<T, Expression<ExprRight>>(scaling, v);
}

template <typename T, typename ExprLeft,
          typename std::enable_if<std::is_scalar<T>::value, bool>::type scalar = true>
const VectorScaling<T, Expression<ExprLeft>>
ALWAYS_INLINE operator/(const Expression<ExprLeft> & v, const T& scaling) {
    return VectorScaling<T, Expression<ExprLeft>>(1. / scaling, v);
}

template <typename ExprLeft, typename ExprRight,
          typename T = typename std::remove_reference_t<ExprLeft>::is_expression>
VectorZip<ExprLeft, ExprRight>
ALWAYS_INLINE operator|(ExprLeft && u, ExprRight && v) {
    return VectorZip<ExprLeft, ExprRight>(u, v);
}

//template <typename ExprLeft, typename ExprRight>
//VectorZip<Expression<ExprLeft>, Expression<ExprRight>>
//ALWAYS_INLINE operator<<(Expression<ExprLeft> & u, Expression<ExprRight> & v) {
//    return VectorZip<Expression<ExprLeft>, Expression<ExprRight>>(u, v);
//}

}
