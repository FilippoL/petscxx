#pragma once

#include <type_traits>

#include "utilities/types.hpp"

namespace petscxx::vector {

using namespace petscxx::base;

class Vector;

template <class Container>
class VectorIterator {
public:
//    VectorIterator() : ptr(nullptr) {
////        vec.get_array();
//    }
    static constexpr bool is_const = std::is_const<Container>::value;

    template <class C = Container, typename std::enable_if<
                  std::is_const<C>::value, bool
                  >::type is_const = true>
    VectorIterator(Container * vec = nullptr,
                   std::ptrdiff_t offset = 0)
        : vec_(vec) {
        if(vec_ != nullptr) {
            vec_->pre_readonly();
            ptr_ = vec_->vec_readonly_array + offset;
        }
    }

    template <class C = Container, typename std::enable_if<
                  !std::is_const<C>::value, bool
                  >::type is_non_const = false>
    VectorIterator(Container * vec = nullptr,
                   std::ptrdiff_t offset = 0)
        : vec_(vec) {
        if(vec_ != nullptr) {
            vec_->pre();
            ptr_ = vec_->vec_array + offset;
        }
    }

    VectorIterator(const VectorIterator & other) = default;

    ~VectorIterator() {
        vec_->post();
    }

    inline scalar_t& operator[](std::ptrdiff_t n) {
        return *(ptr_ + n);
    }

    inline VectorIterator& operator++() {
        ++ptr_;
        return *this;
    }
    inline VectorIterator operator++(int) {
        VectorIterator old(*this);
        ++ptr_;
        return old;
    }
    inline VectorIterator& operator--() {
        --ptr_;
        return *this;
    }
    inline VectorIterator operator--(int) {
        VectorIterator old(*this);
        --ptr_;
        return old;
    }
    inline VectorIterator& operator+=(std::ptrdiff_t n) {
        ptr_ += n;
        return *this;
    }
    inline VectorIterator& operator-=(std::ptrdiff_t n) {
        ptr_ -= n;
        return *this;
    }

    inline VectorIterator operator+(std::ptrdiff_t n) const {
        VectorIterator plus(*this);
        plus.ptr_ += n;
        return plus;
    }
    friend VectorIterator operator+(std::ptrdiff_t n,
                                    const VectorIterator & it) {
        VectorIterator plus(it);
        plus.ptr_ += n;
        return plus;
    }
    inline VectorIterator operator-(std::ptrdiff_t n) const {
        VectorIterator minus(*this);
        minus.ptr_ -= n;
        return minus;
    }
    inline std::ptrdiff_t operator-(const VectorIterator & other) const {
        return this->ptr_ - other.ptr_;
    }

    inline VectorIterator& operator=(const VectorIterator & other) = default;

    inline scalar_t& operator*() {
        return *ptr_;
    }
    inline scalar_t operator*() const {
        return *ptr_;
    }
//    scalar_t& operator->() {
//        return *ptr_;
//    }

    inline bool operator==(const VectorIterator & other) const {
        return /* vec_ == other.vec_ && */ ptr_ == other.ptr_;
    }
    // REMINDER TO SELF: DO NOT CHECK HERE; NOT WORTH IT
    inline bool operator!=(const VectorIterator & other) const {
        return /* vec_ == other.vec_ && */ ptr_ != other.ptr_;
    }
    inline bool operator>=(const VectorIterator & other) const {
        return ptr_ >= other.ptr_;
    }
    inline bool operator>(const VectorIterator & other) const {
        return ptr_ > other.ptr_;
    }
    inline bool operator<=(const VectorIterator & other) const {
        return ptr_ <= other.ptr_;
    }
    inline bool operator<(const VectorIterator & other) const {
        return ptr_ < other.ptr_;
    }
private:
    Container * const vec_ = nullptr;
public:
    using pointer_type =
        std::conditional<is_const, const scalar_t, scalar_t>;
    typename pointer_type::type * ptr_ = nullptr;
};

}
