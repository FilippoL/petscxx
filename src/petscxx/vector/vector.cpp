#include "vector.hpp"

#include "petscsys.h"

#include "base/exceptions.hpp"

#include "vectoriterator.hpp"

namespace petscxx::vector {

Vector::Vector(index_t size,
               Vector::Type vec_type,
               Vector::SubType vec_subtype) {
    if(size > 0) {
        int localSize_ = PETSC_DECIDE;
        PetscSplitOwnership(PETSC_COMM_WORLD, &localSize_, &size);
        localSize = localSize_;
        double* array = (double*)
                        aligned_alloc(32, localSize*sizeof(double));
        VecCreateMPIWithArray(PETSC_COMM_WORLD, 1, localSize,
                              size, array, &vec_);
//        Context::SafeCall(VecCreate(PETSC_COMM_WORLD, &vec_));
//        state_ = State::Created;
        Context::SafeCall(VecSetFromOptions(vec_));
//        state_ = State::HasType;
        state_ = State::HasSize;
    }
//    if(size > 0) {
//        set_size(size);
//    }
}

Vector::Vector(index_t size, scalar_t value,
               Vector::Type vec_type, Vector::SubType vec_subtype)
    : Vector(size, vec_type, vec_subtype) {
    assert(size > 0 && "Cannot value-initialize empty vector.");
    set(value);
}

Vector::Vector(const Vector &other) : Vector() {
    Context::SafeCall(VecDuplicate(other.vec_, &vec_));
    localSize = other.localSize;
    state_ = State::HasSize;
    copy_from(other);
}

Vector::Vector(Vector && other) : Vector() {
    *this = std::move(other);
}

Vector::~Vector() {
    if(ref_count > 0 || ref_readonly_count > 0) {
        std::cout << "Some array was not returned!" << std::endl;
    }
    while(ref_count--) VecRestoreArray(vec_, &vec_array);
    while(ref_readonly_count--) VecRestoreArrayRead(vec_, &vec_readonly_array);

    if(state_ >= State::Created) {
        Context::SafeCall(VecDestroy(&vec_));
    }
}

Vector Vector::duplicate() const {
    Vector v;
    if(!has_type()) throw WrongStateException();
    Context::SafeCall(VecDuplicate(vec_, &v.vec_));
    v.localSize = localSize;
    v.state_ = State::HasSize;
    // Copy elision shall move instead of construct.
    return v;
}

void Vector::create(index_t size = -1) {
    if(is_created()) return;
    if(size >= 0) {
        int localSize_ = PETSC_DECIDE;
        PetscSplitOwnership(PETSC_COMM_WORLD, &localSize_, &size);
        localSize = localSize_;
        double* array = (double*)
                        aligned_alloc(32, localSize*sizeof(double));
        VecCreateMPIWithArray(PETSC_COMM_WORLD, 1, localSize,
                              size, array, &vec_);
//        Context::SafeCall(VecCreate(PETSC_COMM_WORLD, &vec_));
//        state_ = State::Created;
        Context::SafeCall(VecSetFromOptions(vec_));
        state_ = State::HasSize;
    } else {
    //    Context::SafeCall(VecCreate(PETSC_COMM_WORLD, &vec_));
        state_ = State::Created;
        Context::SafeCall(VecSetFromOptions(vec_));
        state_ = State::HasType;
    }
}

bool Vector::is_created() const noexcept {
    if(state_ >= State::Created) return true;
    return false;
}

bool Vector::has_type() const noexcept {
    if(state_ >= State::HasType) return true;
    return false;
}

bool Vector::has_size() const noexcept {
    if(state_ >= State::HasSize) return true;
    return false;
}

void Vector::set(scalar_t value) {
    if(has_size()) {
        Context::SafeCall(VecSet(vec_, value));
    }
}

void Vector::copy_from(const Vector &other) {
    Context::SafeCall(VecCopy(other.vec_, vec_));
}

void Vector::set_size(index_t size) {
    if(!is_created()) create(size);
    else {
        Context::SafeCall(VecSetSizes(vec_, PETSC_DECIDE, size));
        VecGetLocalSize(vec_, &localSize);
    }
    state_ = State::HasSize;
}

void Vector::local_set_size(index_t localSize_) {
    localSize = localSize_;
//    Context::SafeCall(VecSetSizes(vec_, localSize_, PETSC_DECIDE));
    state_ = State::HasSize;
}

void Vector::fill(std::function<scalar_t (index_t)> func) {
    throw NotImplementedException();
}

scalar_t Vector::dot(const Vector &other, bool definite) const {
    scalar_t ret;
    if(definite) Context::SafeCall(VecDot(vec_, other.vec_, &ret));
    else Context::SafeCall(VecTDot(vec_, other.vec_, &ret));
    return ret;
}

std::pair<index_t, scalar_t> Vector::max() const {
    std::pair<index_t, scalar_t> maxv;
    VecMax(vec_, &maxv.first, &maxv.second);
    return maxv;
}

std::pair<index_t, scalar_t> Vector::min() const {
    std::pair<index_t, scalar_t> minv;
    VecMin(vec_, &minv.first, &minv.second);
    return minv;
}

real_t Vector::norm(scalar_t p) const {
    NormType norm_type;
    if(p == 1) norm_type = NORM_1;
    else if(p == 2) norm_type = NORM_2;
    else if(p == std::isinf(p) ) norm_type = NORM_INFINITY;
    else throw NotImplementedException();
    real_t val;
    VecNorm(vec_, norm_type, &val);
    return val;
}

void Vector::scale(scalar_t factor) {
    VecScale(vec_, factor);
}

index_t Vector::size() const noexcept {
    if(!has_size()) return 0;
    index_t n;
    VecGetSize(vec_, &n);
    return n;
}

index_t Vector::local_size() const noexcept {
    if(!has_size()) return 0;
    index_t n;
    VecGetLocalSize(vec_, &n);
    return n;
}

Vec Vector::get_vec() const { return vec_; }



Vector & Vector::operator=(Vector &&other) {
    assert(locked == false && "Cannot move locked Vector!");
    std::swap(other.vec_, vec_);
    std::swap(state_, other.state_);
    std::swap(type_, other.type_);
    std::swap(subtype_, other.subtype_);
    std::swap(localSize, other.localSize);
    return *this;
}

Vector & Vector::operator=(const Vector &other) {
    this->copy_from(other);
    return *this;
}

void Vector::set(index_t idx, scalar_t val, InsertMode mode) {
    VecSetValue(vec_, idx, val, mode);
}

void Vector::pre() {
    if(ref_count++ > 0) {
        return;
    }
    if( ref_readonly_count > 0 ) {
        VecRestoreArrayRead(vec_, &vec_readonly_array);
    }

    VecGetArray(vec_, &vec_array);
    vec_readonly_array = vec_array;
}

void Vector::pre_readonly() const  {
    if(ref_readonly_count++ > 0 || ref_count > 0) return;
    VecGetArrayRead(vec_, &vec_readonly_array);
}

void Vector::post() const {
    if(ref_readonly_count > 0) {
        --ref_readonly_count;
        if(ref_readonly_count == 0 && ref_count == 0) {
            VecRestoreArrayRead(vec_, &vec_readonly_array);
            vec_array = nullptr;
            vec_readonly_array = nullptr;
        }
    } else if (ref_count == 1 ){
        --ref_count;
        VecRestoreArray(vec_, &vec_array);
        vec_array = nullptr;
        vec_readonly_array = nullptr;
    } else if(ref_count > 1) {
        --ref_count;
    }
}

std::ostream & operator<<(std::ostream & o,
                          const Vector &vec) {
    VecView(vec.vec_, PETSC_VIEWER_STDOUT_WORLD);
    return o;
}

}
