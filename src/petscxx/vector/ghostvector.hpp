#pragma once

#include "vector.hpp"

namespace petscxx::vector {

class GhostVector
        : public Vector {
public:
    GhostVector();
};

}
