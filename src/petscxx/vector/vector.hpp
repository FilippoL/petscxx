#pragma once

#include <vector>
#include <algorithm>
#include <functional>
#include <cassert>
#include <future>

// TEST STUFF
#include <chrono>
#include <thread>
#include <iostream>

#include "petscvec.h"

#include "utilities/types.hpp"

#include "base/context.hpp"

#include "vectorexpression.hpp"
#include "vectorextra.hpp"
#include "vectoriterator.hpp"


namespace petscxx::grid {
// StructuredGridVector is forward declared here to allow
// deletion within the move constructor.
template <unsigned int d>
class StructuredGridVector;
}

namespace petscxx::vector {
using namespace petscxx::grid;

// Needed for friend operators.
class Vector;

//!
//! \brief operator << Send Vector to std::ostream
//!
//! Printing is currently done with PetscViewers
//! \param vec
//! \return
//!
std::ostream & operator<<(std::ostream &, const Vector &vec);

//!
//! \brief The Vector class wraps around a Vec PETSc object and provides
//! convenient wrapper member function that safely perform efficient vector
//! operations.
//!
class Vector : public Expression<Vector> {
public:
    //! Alias for iterator on the entire vector data
    using iterator = VectorIterator<Vector>;
    //! Alias for a constant iterator on the entire data
    using const_iterator = VectorIterator<const Vector>;

    //!
    //! \brief The State enum represents the current state of the vector.
    //!
    //! A fully formed vector must have a size and cannot be resize.
    //!
    enum class State {
        Empty, Created, HasType, HasSize
    };
    //!
    //! \brief The Type enum represents the "parallelism" level
    //! of the vector.
    //!
    //!
    enum class Type {
        Auto, MPI, Shared, Sequential
    };
    enum class SubType {
        Standard, Cusp, ViennaCL, CUDA, Nest
    };

    //!
    //! \brief Vector initializes a vector. If a size is specified and the size
    //! is >= 0, then the vector is created/setted up. If a size is specified and the
    //! size is > 0, the the vector is created with that size, with automatic data
    //! distribution.
    //! \param size
    //! \param vec_type
    //! \param vec_subtype
    //!
    Vector(index_t size = -1,
           Type vec_type = Type::Auto,
           SubType vec_subtype = SubType::Standard);
    Vector(index_t size, scalar_t value,
           Type vec_type = Type::Auto,
           SubType vec_subtype = SubType::Standard);
    Vector(const Vector & other);
    Vector(Vector && other);

    //!
    //! \brief Vector constructor that prevents moving
    //! and object of derived to base.
    //! \param other
    //!
    template <unsigned int d>
    Vector(StructuredGridVector<d> && other) = delete;

    virtual ~Vector();

    //!
    //! \brief create calls VecCreate/VecSetFromOptions.
    //!
    //! The function does that the constructor does, if a size of
    //! >= 0 is specified. However, the function is not const-qualified,
    //! since internal data is modified.
    //!
    void create(index_t size);

    //!
    //! \brief duplicate creates a new, movable object,
    //! that dupliates the original object. No copy of
    //! internal values is actually performed.
    //! \return
    //!
    Vector duplicate() const;

    bool is_created() const noexcept;
    bool has_type() const noexcept;
    bool has_size() const noexcept;

    void copy_from(const Vector & other);

    void set(scalar_t value);
    void fill(std::function<scalar_t(index_t)> func);

    void set_size(index_t size);
    void local_set_size(index_t localSize);

    scalar_t dot(const Vector &other, bool definite = true) const;

    template <unsigned int offset = 0U,
              class T,
              long unsigned int size>
    void unpack(std::array<Vec, size> & arr,
                const T & single) const {
        std::get<offset>(arr) = single.vec_;
    }

    template <unsigned int offset = 0U,
              class T, class ...Args,
              long unsigned int size>
    void unpack(std::array<Vec, size> & arr,
                const T & single, const Args &... args) const {
        std::get<offset>(arr) = single.vec_;
        unpack<offset+1U>(arr, args...);
    }

    template <bool defined = true, class T, class... Args>
    auto dot(const T & first, const Args &... args) const {
        constexpr int size = sizeof...(Args) + 1;
        std::array<scalar_t, size> vals;

        std::array<Vec, size> others_vec_;

        unpack(others_vec_, first, args...);

        if (defined)
                Context::SafeCall(
                    VecMDot(vec_, size, others_vec_.data(), vals.data())
                    );
        else
        Context::SafeCall(
                    VecMTDot(vec_, size, others_vec_.data(), vals.data())
                    );
        return vals;
    }

    std::pair<index_t, scalar_t> max() const;
    auto maxv() const { return max().second; }
    std::pair<index_t, scalar_t> min() const;
    auto minv() const { return min().second; }

    real_t norm(scalar_t p = 2) const;

    void scale(scalar_t factor);

    index_t size() const noexcept;
    index_t local_size() const noexcept;

    Vec get_vec() const;

    Vector& operator=(Vector && other);
    Vector& operator=(const Vector & other);

    ALWAYS_INLINE __m256d& evalat(int i) {
        return *((__m256d*) (vec_array + i));
    }
    ALWAYS_INLINE __m256d evalat(int i) const {
        return *((__m256d*) (vec_readonly_array + i));
    }
    static constexpr int packetSize = 4;

    template <typename Expr>
    inline Vector& operator=(const Expression<Expr> & expr) {
        assert(has_size() && "Assigned vector has no size!");
        index_t nlocal;
        VecGetLocalSize(vec_, &nlocal);
        pre();
        expr().pre_readonly();

        if(!is_aligned(vec_array, 32)) std::cout << "Misalignment!";

        for (index_t i = 0; i+packetSize < /* FUCK!!! */ nlocal; i+=packetSize) {
            *((__m256d*) (vec_array + i)) = expr().evalat(i);
        }
        int residual = nlocal % packetSize;
        while(residual > 0) {
            vec_array[nlocal - residual] = expr[nlocal - residual];
            --residual;
        }

        post();
        expr().post();
        vec_array = nullptr;

        return *this;
    }

    template <typename Expr>
    inline Vector& operator+=(const Expression<Expr> & expr) {
        assert(has_size() && "Assigned vector has no size!");
        index_t nlocal;
        VecGetLocalSize(vec_, &nlocal);
        pre();
        expr().pre_readonly();
        for (index_t i = 0; i+packetSize < /* FUCK!!! */ nlocal; i+=packetSize) {
            *((__m256d*) (vec_array + i)) += expr.evalat(i);
        }
        int residual = nlocal % packetSize;
        while(residual > 0) {
            vec_array[nlocal - residual] += expr[nlocal - residual];
            --residual;
        }

        post();
        expr().post();
        vec_array = nullptr;

        return *this;
    }

    template <typename T>
    Vector& operator+=(const VectorScaling<T, Expression<Vector>> & scale_expr) {
        VecAXPY(vec_, scale_expr.left_expr_, scale_expr.right_expr_().vec_);
        return *this;
    }

    void set(index_t idx, scalar_t val,
             InsertMode mode);

    //!
    //! \brief set inserts/adds value to a vector
    //! in a "global" manner, and in a streamlined
    //! fashion.
    //! \param indices
    //! \param values
    //! \param mode
    //!
    template <class IndexContainer,
              class ValueContainer>
    void set(const IndexContainer & indices,
             const ValueContainer & values,
             InsertMode mode) {
        assert(indices.size() == values.size() &&
               "Indices/Values must have the same size!");
        VecSetValues(vec_, indices.size(),
                     indices.data(), values.data(), mode);
        dirty = true;
    }

    //!
    //! \brief assemble set values and transfer those
    //! to all relevant processes. The call is non-blocking
    //! as long as the return values in not destructed.
    //!
    //! Returns a "future"-like object that will finalize the transfer
    //! on destruction (or wait/get). Waiting for the future
    //! will lock the loop until transfer is complete.
    //!
    auto assemble() const {
        return make_pre_post_future(
                    [this] () {
            VecAssemblyBegin(vec_);
            locked = true;
        },
        [this] () {
            VecAssemblyEnd(vec_);
            dirty = false;
            locked = false;
        }
        );
    }

    //!
    //! \brief get_element_wise_access
    //! obtains array and create a "future"-like
    //! object that automatically restore the array
    //! on deletion.
    //!
    auto get_element_wise_access() {
        return make_pre_post_future(
                    [this] () {
            pre();
        },
        [this] () {
            post();
        }
        );
    }

    //!
    //! \brief get_element_wise_readonly_access
    //! obtains array and create a "future"-like
    //! object that automatically restore the array
    //! on deletion. Read-only version.
    //!
    auto get_element_wise_readonly_access() const {
        return make_pre_post_future(
                    [this] () {
            pre_readonly();
        },
        [this] () {
            post();
        }
        );
    }

    //!
    //! \brief lock sets the Vector in a locked state,
    //! in which its internal data shall not be modified.
    //!
    //! A Vector is locked whenever some asyncronous operation
    //! is in progress.
    //!
    void lock() { locked = true; }
    void unlock() { locked = false; }

    friend std::ostream& operator<<(std::ostream & o, const Vector & vec);
    friend VectorIterator<Vector>;
    friend VectorIterator<const Vector>;
public: // protected: ?
    //!
    //! \brief operator [] provies constant read-only value
    //! random access. A call to pre or pre-readonly
    //! must have been done.
    //! \param i
    //! \return
    //!
    inline double operator[](size_t i) const
    /*__attribute__((aligned(16))) __attribute__((assume_aligned(16)))*/ {
        return vec_readonly_array[i];
    }

    //!
    //! \brief operator [] provies non-constant read-write
    //! reference random access. A call to pre must have been done.
    //! \param i
    //! \return
    //!
    inline double &operator[](size_t i)
    /*__attribute__((aligned(16))) __attribute__((assume_aligned(16)))*/ {
        return vec_array[i];
    }

    //!
    //! \brief pre obtains the array for element-wise
    //! manipulations. Obtained array is read-write.
    //!
    //! After this call, the state will be "locked".
    //!
    void pre();
    //!
    //! \brief pre_readonly obtains the array for element-wise
    //! manupulations. The obtained array is read-only.
    //!
    //! After this call, the state will be "locked".
    //!
    void pre_readonly() const;
    //!
    //! \brief post restores the array used for element-wise
    //! manupulations. The obtained array will be null.
    //!
    //! The state will be unlocked. All pointers are reset.
    //!
    //! TODO: we may want to count a reference to pre/pre_readonly
    //! before restoring the array, this way we have futures
    //! that work if shared.
    //!
    void post() const;

    inline iterator begin() {
        return iterator(this, get_begin());
    }

    inline iterator end() {
        return iterator(this, get_end());
    }

    inline const_iterator begin() const {
        return const_iterator(this, get_begin());
    }

    inline const_iterator end() const {
        return const_iterator(this, get_end());
    }


    inline index_t get_begin() const {
        return 0;
    }
    inline index_t get_end() const {
//        index_t nlocal;
//        VecGetLocalSize(vec_, &nlocal);
        return localSize; //nlocal;
    }

    //! If any array is owned by somebody else, then no operation
    //! shall be performed on the array.
    bool owned_by_other() const { return ref_count > 0 || ref_readonly_count > 0; }
public:
    scalar_t* get_array() {return vec_array; }
    const scalar_t* get_array_read() const {return vec_readonly_array; }

protected:
    //! Internal pointer to object of type Vec.
    Vec vec_;
    //! What state the vector is currently in.
    State state_ = State::Empty;
    //! Type of the vector, e.g. standard, MPI, or shared.
    Type type_;
    //! Subtype of the vector, e.g. CUDA, ViennaCL, ...
    SubType subtype_;
    //! Back this up for iteration usage
    int localSize = -1;

    //! Pointer to internal structure for read-write access.
    //! Vector is locked when this is not nullptr.
    //! Must call pre/post to use.
    mutable scalar_t* vec_array /*__attribute__((aligned(16)))*/ = nullptr;
    //! Pointer to internal structure for read-only access.
    //! Vector is locked when this is not nullptr.
    //! Must call pre/pre_readonly/post to use.
    mutable const scalar_t* vec_readonly_array /*__attribute__((aligned(16)))*/ = nullptr;
    //! When has_array is true, the Vector's Vec has ownership of its internal
    //! data, meaning that only him can perform operations on this data.
    //! When this is true, vec_array shall be non-null. Otherwise vec_array
    //! will point to the sequential memory location owned by the vector.
    mutable int ref_count = 0;
    mutable int ref_readonly_count = 0;

    //! A vector is locked between a call of VecAssemblyBegin and
    //! the call to VecAssemblyEnd. When a vector is locked,
    //! no operation shall be perfomed on the internal storage
    //! of the vector.
    mutable bool locked = false;
    //! If the Vector is dirty, it means some value has not been
    //! updated, generally a call to VecAssemblyBegin/VecAssemblyEnd
    //! did not take place.
    mutable bool dirty = false;
};

}
