#pragma once

namespace petscxx::vector {

template <class T>
class pre_post_future {
public:
    using ret_T = typename std::result_of<T()>::type;

    template <class L>
    pre_post_future(const L& pre,
                    const T& post)
        : post_(post) {
        pre();
    }

    pre_post_future(const pre_post_future<T>& other) = delete;
    pre_post_future(pre_post_future<T>&& other)
        : post_(std::move(other.post_))
        , ret_(std::move(other.ret_)) {
    }
    ~pre_post_future() {
        wait();
    }

    template <class C = ret_T,
              std::enable_if_t<std::is_same<C, void>::value, bool>
                    is_void = true>
    C get() {
        wait();
        return;
    }

    template <class C = ret_T,
              std::enable_if_t<!std::is_same<C, void>::value, bool>
                    is_void = false>
    C get() {
        wait();
//        constexpr if(!std::is_same<ret_T, void>::value) {
        return ret_;
//        }
    }

    template <class C = ret_T,
              std::enable_if_t<std::is_same<C, void>::value, bool>
                    is_void = true>
    void wait() {
        if(!has_value_) {
            post_();
            has_value_ = true;
        }
    }

    template <class C = ret_T,
              std::enable_if_t<!std::is_same<C, void>::value, bool>
                    is_void = false>
    void wait() {
//        constexpr if(!std::is_same<ret_T, void>::value) {
        if(!has_value_) {
            ret_ = post_();
            has_value_ = true;
        }
//        }
    }

private:
    template <class V>
    using dummy_if_void = typename std::conditional<
                                        std::is_same<V, void>::value, int, V
                                   >::type;

    T post_;
    dummy_if_void<ret_T> ret_;
    bool has_value_ = false;
};

template <class T, class L>
auto make_pre_post_future(const L& pre,
                          const T& post) {
    return pre_post_future<
            typename std::remove_reference<T>::type
           >(pre, post);
}

}
