#pragma once

#include "vector/vectoriterator.hpp"

namespace petscxx::grid {

using namespace petscxx::vector;

template<class Container, unsigned int d>
class StructuredGridVectorIterator
        : public VectorIterator<Container> {
public:
    StructuredGridVectorIterator() {}
};

}
