#pragma once

#include <array>
#include <vector>
#include <cassert>
#include <iostream>
#include <algorithm>

#include "petscdmda.h"

#include "base/context.hpp"
#include "utilities/types.hpp"
#include "utilities/mpi.hpp"
#include "utilities/operators.hpp"

namespace petscxx::grid {

using namespace petscxx::base;
using namespace petscxx::utilities;

template<unsigned int d>
class StructuredGridVector;

template <unsigned int d>
class StructuredGrid {
public:
    struct Stencil {
    public:
        Stencil(int size, DMDAStencilType type)
            : size(size), type(type) {}

        int size;
        DMDAStencilType type;

    };

    static_assert(d == 1 || d == 2 || d == 3,
                  "Only dimension 1,2, and 3 are available.");

    StructuredGrid(const std::array<DMBoundaryType, d> & bdrys,
                   const std::array<index_t, d> & shape,
                   std::array<index_t, d> numberOfRanks_,
                   Stencil stencil,
                   std::array<std::vector<int>, d> ownershipRange,
                   int dof = 1,
                   MPI_Comm comm = Context::World)
        : stencil(stencil), shape(shape), dof(dof), numberOfRanks(numberOfRanks_) {


        int * own1 = ownershipRange[0].size() == 0 ?
                         nullptr : ownershipRange[0].data(), *own2, *own3;
        /* constexpr */ if(d >= 2) {
            own2 = ownershipRange[1].size() == 0 ?
                                 nullptr : ownershipRange[1].data();
        }
        /* constexpr */ if(d >= 3) {
            own3 = ownershipRange[2].size() == 0 ?
                                 nullptr : ownershipRange[2].data();
        }

        /* constexpr */ if(d == 1) {
            DMDACreate1d(comm, bdrys[0], shape[0],
                         dof, stencil.size,
                         own1, &da);
        } else if(d == 2) {
            DMDACreate2d(comm, bdrys[0], bdrys[1], stencil.type,
                         shape[0], shape[1],
                         numberOfRanks_[0], numberOfRanks_[1],
                         dof, stencil.size,
                         own1, own2, &da);
        } else if(d == 3) {
            DMDACreate3d(comm, bdrys[0], bdrys[1], bdrys[2], stencil.type,
                         shape[0], shape[1], shape[2],
                         numberOfRanks_[0], numberOfRanks_[1], numberOfRanks_[2],
                         dof, stencil.size,
                         own1, own2, own3, &da);
        }

        DMDAGetInfo(da, nullptr /* dim */, nullptr, nullptr, nullptr, /* shape */
                    &numberOfRanks[0], d >= 2 ? &numberOfRanks[1] : nullptr, d >= 3 ? &numberOfRanks[2] : nullptr,
                    nullptr /* dof */, nullptr /* stencil */,
                    nullptr, nullptr, nullptr /* boundaries */, nullptr /* stencilType */);
    }

    StructuredGrid(const std::array<DMBoundaryType, d> & bdrys,
                   const std::array<index_t, d> & shape,
                   std::array<index_t, d> numberOfRanks,
                   Stencil stencil,
                   int dof = 1,
                   MPI_Comm comm = Context::World)
        : StructuredGrid(bdrys, shape, numberOfRanks,
                         stencil,
                         {std::vector<int>{}, std::vector<int>{}, std::vector<int>{}},
                         dof, comm) {

    }

    StructuredGrid(const std::array<DMBoundaryType, d> & bdrys,
                   const std::array<index_t, d> & shape,
                   Stencil stencil,
                   int dof = 1,
                   MPI_Comm comm = Context::World)
        : StructuredGrid(bdrys, shape,
                         {PETSC_DECIDE, PETSC_DECIDE, PETSC_DECIDE},
                         stencil, dof, comm) {
    }

    ~StructuredGrid() {
        DMDestroy(&da);
    }

    void set_fields_names(const std::vector<std::string> & names) {
        assert(names.size() == (unsigned int) dof && "Wrong number of names.");
        std::vector<const char *> cstrnames(dof+1);
        std::transform(names.begin(), names.end(), cstrnames.begin(),
                       [] (const std::string & s) -> const char * { return s.c_str(); }
        );
        cstrnames.back() = nullptr;

        DMDASetFieldNames(da, cstrnames.data());
    }

    //    void set_ownership_range(std::array<std::vector<int>, d> own) {
    //        DMDASetOwnershipRanges(da, own[0].data(), own[1].data(), own[2].data());
    //    }

    void print() {
        std::cout << "StructuredGrid with dim = " << d << ", dof = " << dof << "\n"
                  << " - shape:         " << shape << "\n"
                  << " - numberOfRanks: " << numberOfRanks << "\n"
                  << " - stencil:       " << stencil.size << " " << stencil.type << std::endl;
    }

    DM get_dm() const {
        return da;
    }

    StructuredGridVector<d> create_vector(typename StructuredGridVector<d>::VectorType vector_type) const {
        return StructuredGridVector<d>(*this, vector_type, false);
    }

    StructuredGridVector<d> get_vector(typename StructuredGridVector<d>::VectorType vector_type) const {
        return StructuredGridVector<d>(*this, vector_type, true);
    }

    //!
    //! \brief restore_vector restores a vector. It is there just for
    //! symmetry reason, but it should be unecessary, since a call
    //! to vec.restore() should be sufficient.
    //! \return
    //!
    StructuredGridVector<d> restore_vector(const StructuredGridVector<d> & vec) const {
        vec.restore();
    }


    const Stencil stencil;
    const static int dim = d; // one, two or three dimensional
    const std::array<int, 3> shape;
    const int dof;
    std::array<int, d> numberOfRanks;
//    std::array<int, d> localSize;
private:
    DM da;
};

}
