#pragma once

#include "vector/vector.hpp"
#include "structuredgrid.hpp"

#include "base/exceptions.hpp"

namespace petscxx::grid {

using namespace petscxx::vector;

template <unsigned int d>
class StructuredGridVector : public Vector {
public:
    enum class VectorType {
        Global, Local, Natural
    };

    StructuredGridVector(const StructuredGrid<d> & grid,
                         VectorType vector_type = VectorType::Global,
                         bool use_buffer = false)
        : grid_(grid), vector_type_(vector_type), use_buffer_(use_buffer) {
        if( use_buffer ) {
            switch (vector_type_) {
                case VectorType::Global:
                default:
                    DMGetGlobalVector(grid_.get_dm(), &vec_);
                    break;
                case VectorType::Local:
                    DMGetLocalVector(grid_.get_dm(), &vec_);
                    break;
                case VectorType::Natural:
                    throw NotImplementedException();
                    break;
            }
        } else {
            switch (vector_type_) {
                case VectorType::Global:
                default:
                    DMCreateGlobalVector(grid_.get_dm(), &vec_);
                    break;
                case VectorType::Local:
                    DMCreateLocalVector(grid_.get_dm(), &vec_);
                    break;
                case VectorType::Natural:
                    DMDACreateNaturalVector(grid_.get_dm(), &vec_);
                    break;
            }
        }
        state_ = State::HasSize;
    }

    StructuredGridVector(const StructuredGridVector<d> & other) = default;
    StructuredGridVector(StructuredGridVector<d> && other) = default;

    virtual ~StructuredGridVector() {
        if(use_buffer_) {
            restore();
        }
    }

    void restore() {
        if(use_buffer_) {
            switch (vector_type_) {
                case VectorType::Global:
                default:
                    DMRestoreGlobalVector(grid_.get_dm(), &vec_);
                    break;
                case VectorType::Local:
                    DMRestoreLocalVector(grid_.get_dm(), &vec_);
                    break;
                case VectorType::Natural:
                    throw NotImplementedException();
                    break;
            }

            use_buffer_ = false;
            state_ = State::Empty;
        }
    }

    // void set(scalar_t val, int dof);
    // void set(std::array<scalar_t, d> vals);
    // void stride();
    // apply(Function f(x,y,z));
    // iterator with up down etc...
    // operator[] random access? -> to Vector
    // operator(x,y,z) structured random access?
    // get_array() -> vec
    // restore_array(); -> vec

    StructuredGridVector<d>& operator=(const StructuredGridVector<d> & other) {
        static_cast<Vector&>(*this) = static_cast<const Vector&>(other);
        return *this;
    }

    template <typename Expr>
    StructuredGridVector<d>& operator=(const Expression<Expr> & expr) {
        static_cast<Vector&>(*this) = expr;
        return *this;
    }

private:
    const StructuredGrid<d> & grid_;

    VectorType vector_type_;
    bool use_buffer_ = false;
};

}
