#pragma once

#include "petscviewer.h"

#include "base/context.hpp"
#include "base/exceptions.hpp"

#include "vector/vector.hpp"

namespace petscxx::io {

using namespace petscxx::vector;

class Viewer {
public:
//    PetscViewerDrawOpen(PETSC_COMM_WORLD,NULL,NULL,0,0,300,300,&viewer);
//     85:   PetscViewerPushFormat(viewer,PETSC_VIEWER_DRAW_LG);

    Viewer(MPI_Comm comm = Context::World) noexcept
        : comm_(comm) {
        create();
    }
    Viewer(std::string filename, std::ios_base::openmode mode,
           MPI_Comm comm = Context::World)
        : Viewer(comm) {
        open(filename, mode);
    }

    ~Viewer() {
        if(viewer_) {
            close();
        }
    }

    void create() {
        PetscViewerCreate(comm_, &viewer_) ;

//#define PETSCVIEWERSOCKET       "socket"
//#define PETSCVIEWERASCII        "ascii"
//#define PETSCVIEWERBINARY       "binary"
//#define PETSCVIEWERSTRING       "string"
//#define PETSCVIEWERDRAW         "draw"
//#define PETSCVIEWERVU           "vu"
//#define PETSCVIEWERMATHEMATICA  "mathematica"
//#define PETSCVIEWERNETCDF       "netcdf"
//#define PETSCVIEWERHDF5         "hdf5"
//#define PETSCVIEWERVTK          "vtk"
//#define PETSCVIEWERMATLAB       "matlab"
//#define PETSCVIEWERSAWS          "saws"
//        PetscViewerSetType(viewer_, PETSCVIEWERSOCKET); -->
//        PetscViewerSetType(viewer_, PETSCVIEWERASCII);
        PetscViewerSetType(viewer_, PETSCVIEWERBINARY);
//          PetscViewerSetType(viewer_, PETSCVIEWERSTRING);
//        PetscViewerSetType(viewer_, PETSCVIEWERDRAW); (( draw?
//        PetscViewerSetType(viewer_, PETSCVIEWERVU); -->
//        PetscViewerSetType(viewer_, PETSCVIEWERMATHEMATICA); -->
//        PetscViewerSetType(viewer_, PETSCVIEWERNETCDF); -->
//        PetscViewerSetType(viewer_, PETSCVIEWERHDF5); -->
//        PetscViewerSetType(viewer_, PETSCVIEWERVTK); // (FILE NAME)
//        PetscViewerSetType(viewer_, PETSCVIEWERMATLAB); // -->
//        PetscViewerSetType(viewer_, PETSCVIEWERSAWS); -->

//      PetscViewerBinarySetSkipHeader(viewer_, PETSC_TRUE);
    }

    bool open(std::string filename, std::ios_base::openmode mode) {
        if(!viewer_) create();
        mode_ = mode;
        switch (mode_) {
        case std::ios_base::out:
            PetscViewerFileSetMode(viewer_, FILE_MODE_WRITE);
            break;
        case std::ios_base::in:
            PetscViewerFileSetMode(viewer_, FILE_MODE_READ);
            break;
        default:
            throw new std::exception();
            break;
        }
        PetscObjectSetName((PetscObject) viewer_, "Line graph Plot");
        PetscViewerBinarySetUseMPIIO(viewer_, PETSC_TRUE);
        PetscViewerFileSetName(viewer_, filename.c_str());

        return true;
    }

    void write(const Vector & vec) {
        VecView(vec.get_vec(), viewer_);
    }

    void read(Vector & vec) {
        VecLoad(vec.get_vec(), viewer_);
    }

    void operator<<(bool) {
        throw NotImplementedException();
    }
    void operator>>(bool) {
        throw NotImplementedException();
    }

    void close() {
        PetscViewerDestroy(&viewer_);
        viewer_ = nullptr;
    }

private:
    PetscViewer viewer_;
    std::ios_base::openmode mode_;
    MPI_Comm comm_;
};

}
