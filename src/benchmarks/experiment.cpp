#include <iostream>
#include <vector>
#include <cassert>

#include "benchmark/benchmark_api.h"

#include "utilities/simd.hpp"
#include "utilities/alignment.hpp"

constexpr int min_size = 8;
constexpr int max_size = 8*8*8*8*8*8*8;

//static void BM_512d(benchmark::State& state) {
//    int size = state.range(0);
//    double alpha = 8., beta = 6., gamma = 5.;
//    std::vector<double> x(size+8), y(size+8), z(size+8);
//    double *x_ = x.data(),
//           *y_ = y.data(),
//           *z_ = z.data();
//    align(&x_, 64);
//    align(&y_, 64);
//    align(&z_, 64);

////    std::cout << x_ << " " << y_ << " " << z_ << std::endl;
//    while (state.KeepRunning()) {
//        volatile __m512d a = {0.,0.,0.,0.,0.,0.,0.,0.};
//        for(int i = 0; i < size; i+=8) {
//            (*(__m512d*) (x_+i)) = _mm512_add_pd(*(__m512d*) (z_+i), *(__m512d*) (y_+i));
//        }
////        assert(a[0]+a[1]+a[2]+a[3] == 0);
//    }
//    state.SetComplexityN(state.range(0));
//}
//BENCHMARK(BM_512d)->Range(min_size, max_size)->Complexity(benchmark::oN);

template <unsigned int byte_width, unsigned int misalignment = 0>
class Kernel {
public:
    Kernel(double*x_, double*y_, double*z_, int size)
        : end(size), x_(x_), y_(y_), z_(z_) {
        std::cout << alignof(double*);
    }

    inline void eval(int i) {
        (*(__m256d*) (x_+i)) = _mm256_add_pd(*(__m256d*) (z_+i), *(__m256d*) (y_+i));
    }
    inline void evals(int i) {
        x_[i] = y_[i] + z_[i];
    }

    inline void pre() {
//        /* constexpr */ if(int misalignment_ = misalignment; misalignment == -1) {
//        int misalignment_ = misalignment;
        if(misalignment == -1) {
            int misalignment_ = misalignment_of(x_, byte_width);
            for(int misal = 0; misal < misalignment_; ++misal) {
                evals(misal);
            }
        }
    }

    inline void post() {
        for(int rest = end % step; rest > 0; --rest) {
            evals(end-rest);
        }
    }

    int start = 0;
    int end;
    constexpr static int step = byte_width / sizeof(double);

    double *x_,*y_,*z_;
};

static void BM_Kernel(benchmark::State& state) {
    int size = state.range(0);
//    double alpha = 8., beta = 6., gamma = 5.;
    std::vector<double> x(size+4), y(size+4), z(size+4);
    double *x_ = x.data(),
           *y_ = y.data(),
           *z_ = z.data();
    realign(&x_, 32);
    realign(&y_, 32);
    realign(&z_, 32);

//    std::cout << x_ << " " << y_ << " " << z_ << std::endl;
    while (state.KeepRunning()) {
        Kernel<32> kernel(x_, y_, z_,size);
        kernel.pre();
        for(int i = kernel.start; i < kernel.end; i+=kernel.step) {
            kernel.eval(i);
        }
        kernel.post();
    }
    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_Kernel)->Range(min_size, max_size)->Complexity(benchmark::oN);

static void BM_256d(benchmark::State& state) {
    int size = state.range(0);
//    double alpha = 8., beta = 6., gamma = 5.;
    std::vector<double> x(size+4), y(size+4), z(size+4);
    double *x_ = x.data(),
           *y_ = y.data(),
           *z_ = z.data();
    realign(&x_, 32);
    realign(&y_, 32);
    realign(&z_, 32);

//    std::cout << x_ << " " << y_ << " " << z_ << std::endl;
    while (state.KeepRunning()) {
//        volatile __m256d a = {0.,0.,0.,0.};
        for(int i = 0; i < size; i+=4) {
            (*(__m256d*) (x_+i)) = _mm256_add_pd(*(__m256d*) (z_+i), *(__m256d*) (y_+i));
        }
//        assert(a[0]+a[1]+a[2]+a[3] == 0);
    }
    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_256d)->Range(min_size, max_size)->Complexity(benchmark::oN);

static void BM_128d(benchmark::State& state) {
    int size = state.range(0);
//    double alpha = 8., beta = 6., gamma = 5.;
    std::vector<double> x(size+1, 1), y(size+1, 1), z(size+1, 1);
    double *x_ = x.data(),
           *y_ = y.data(),
           *z_ = z.data();
    realign(&x_);
    realign(&y_);
    realign(&z_);

    __m128d a = {0.,0.};
    while (state.KeepRunning()) {
        a[0] = 0.; a[1] = 0.;
        for(int i = 0; i < size; i+=2) {
            *(__m128d*) (x_+i) = _mm_add_pd(*(__m128d*) (z_+i), *(__m128d*) (y_+i));
        }
    }
//    if(a[0] + a[1] != 2*size) std::cout << "Err1 " << a[0]+a[1] << " " << 2*size << std::endl;
//    assert(a[0] + a[1] == 2*size);
    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_128d)->Range(min_size, max_size)->Complexity(benchmark::oN);

static void BM_normal(benchmark::State& state) {
    int size = state.range(0);
//    double alpha = 8., beta = 6., gamma = 5.;
    std::vector<double> x(size+1, 1), y(size+1, 1), z(size+1, 1);
    double *x_ = x.data(),
           *y_ = y.data(),
           *z_ = z.data();
    realign(&x_);
    realign(&y_);
    realign(&z_);

//    double a;
    while (state.KeepRunning()) {
//        a = 0.;
        for(int i = 0; i < size; ++i) {
            z_[i] = x_[i] + y_[i];
        }
    }
//    if(a != 2*size) std::cout << "Err1 " << a << " " << 2*size << std::endl;
//    assert(a == 2*size);
    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_normal)->Range(min_size, max_size)->Complexity(benchmark::oN);

BENCHMARK_MAIN()//;
