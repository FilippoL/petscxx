if (CMAKE_VERSION VERSION_LESS 3.2)
    set(UPDATE_DISCONNECTED_IF_AVAILABLE "")
else()
    set(UPDATE_DISCONNECTED_IF_AVAILABLE "UPDATE_DISCONNECTED 1")
endif()

set(BENCHMARKS_ENABLE_TESTING OFF)

find_package(Threads REQUIRED)

if(ENABLE_EIGEN)
    find_package(Eigen3 REQUIRED)
    add_definitions(-DENABLE_EIGEN)
    include_directories(${EIGEN3_INCLUDE_DIR})
endif(ENABLE_EIGEN)

include(${CMAKE_SOURCE_DIR}/cmake-modules/DownloadProject.cmake)
download_project(PROJ                benchmark
                 GIT_REPOSITORY      https://github.com/google/benchmark.git
                 GIT_TAG             master
                 ${UPDATE_DISCONNECTED_IF_AVAILABLE}
)
add_subdirectory(${benchmark_SOURCE_DIR} ${benchmark_BINARY_DIR})

macro(add_benchmark name)
    include_directories(${PETSCXX_INCLUDES} ${PETSC_INCLUDES}
        ${MPI_CXX_INCLUDE_PATH} ${MPI_C_INCLUDE_PATH} ${benchmark_SOURCE_DIR}/include)
    add_executable(${name} ${name}.cpp)
    target_link_libraries(${name} benchmark petscxx_base pthread
        ${PETSC_LIBRARIES}
        ${Boost_LIBRARIES} ${MPI_CXX_LIBRARIES} ${MPI_C_LIBRARIES}
        ${Boost_UNIT_TEST_FRAMEWORK_LIBRARY} ${CMAKE_THREAD_LIBS_INIT})
    add_test(NAME ${name} COMMAND ${name})
endmacro(add_benchmark name)

add_benchmark(bench_vector)
add_benchmark(bench_loops)
add_benchmark(experiment)
