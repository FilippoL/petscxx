#include "benchmark/benchmark_api.h"

#include <iostream>

#include <utilities/math.hpp>

#include <base/context.hpp>

#include <vector/vector.hpp>
#include <vector/vectorexpression.hpp>
#include <vector/vectoriterator.hpp>

constexpr int min_size = 8;
constexpr int max_size = petscxx::utilities::power<6>(8);

std::unique_ptr<petscxx::base::Context> ctx;

static void BM_Vector_beginend(benchmark::State& state) {
    if(ctx == nullptr) ctx = std::make_unique<petscxx::base::Context>();

    int size = state.range(0);
    petscxx::vector::Vector u(size), w(size);

    u.set(6);
    while (state.KeepRunning()) {
        int count = 0;
        auto end = u.end();
        for(auto it = u.begin(); it < end; ++it) {
            ++count;
            *it = count;
        }
        if(count != size) std::cout << "Size error!";
    }

    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_Vector_beginend)->Range(min_size, max_size)->Complexity(benchmark::oN);

static void BM_Vector_foreach(benchmark::State& state) {

    int size = state.range(0);
    petscxx::vector::Vector u(size), w(size);

    u.set(6);
    while (state.KeepRunning()) {
        int count = 0;
        for(auto& val: u) {
            ++count;
            val = count;
        }
        if(count != size) std::cout << "Size error!";
    }

    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_Vector_foreach)->Range(8, max_size)->Complexity(benchmark::oN);

static void BM_Vector_getarray(benchmark::State& state) {

    int size = state.range(0);
    petscxx::vector::Vector u(size), w(size);

    u.set(6);
    while (state.KeepRunning()) {
        int count = 0;
        auto fut = u.get_element_wise_access();
        int end = u.get_end();
        for(int i = u.get_begin(); i < end; ++i) {
            ++count;
            u[i] = count;
        }
        fut.get();
        if(count != size) std::cout << "Size error!";
    }

    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_Vector_getarray)->Range(min_size, max_size)->Complexity(benchmark::oN);

static void BM_PETSc(benchmark::State& state) {

    int size = state.range(0);
    petscxx::vector::Vector u(size), w(size);

    u.set(6);
    while (state.KeepRunning()) {
        int count = 0;
        double * array;
        VecGetArray(u.get_vec(), &array);
        int localSize;
        VecGetLocalSize(u.get_vec(), &localSize);
        for(int i = 0; i < localSize; ++i) {
            ++count;
            array[i] = count;
        }
        VecRestoreArray(u.get_vec(), &array);
        if(count != size) std::cout << "Size error!";
    }

    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_PETSc)->Range(min_size, max_size)->Complexity(benchmark::oN);

BENCHMARK_MAIN()//;
