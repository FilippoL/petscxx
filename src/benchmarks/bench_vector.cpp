#include "benchmark/benchmark_api.h"

#include <iostream>

#include <utilities/math.hpp>

#include <base/context.hpp>

#include <vector/vector.hpp>
#include <vector/vectorexpression.hpp>
#include <vector/vectoriterator.hpp>

#include <io/viewer.hpp>

#if ENABLE_EIGEN
#    include <Eigen/Dense>
#endif

constexpr int min_size = 8;
constexpr int max_size = petscxx::utilities::power<6>(8);

std::unique_ptr<petscxx::base::Context> ctx;

static void BM_Vector_petscxx_AXPY(benchmark::State& state) {
    if(ctx == nullptr) ctx = std::make_unique<petscxx::base::Context>();

    int size = state.range(0);
    double alpha = 8.;
    petscxx::vector::Vector v(size), w(size);

//    std::cout << "32-aligned: " << is_aligned(w.begin().ptr_, 32) <<std::endl;

    while (state.KeepRunning()) {
        v += alpha * w;
    }
    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_Vector_petscxx_AXPY)->Range(min_size, max_size)->Complexity(benchmark::oN);

static void BM_Vector_PETSc_AXPY(benchmark::State& state) {
    Vec v, w;
    VecCreate(MPI_COMM_WORLD, &v);
    VecCreate(MPI_COMM_WORLD, &w);
    VecSetFromOptions(v);
    VecSetFromOptions(w);
    VecSetSizes(v, PETSC_DECIDE, state.range(0));
    VecSetSizes(w, PETSC_DECIDE, state.range(0));
    double alpha = 8.;

    while (state.KeepRunning()) {
        VecAXPY(w, alpha, v);
    }
    state.SetComplexityN(state.range(0));

    VecDestroy(&v);
    VecDestroy(&w);
}
BENCHMARK(BM_Vector_PETSc_AXPY)->Range(8, max_size)->Complexity(benchmark::oN);

#if ENABLE_EIGEN
static void BM_Vector_Eigen_AXPY(benchmark::State& state) {
    int size = state.range(0);
    double alpha = 8.;
    Eigen::VectorXd v(size), w(size);
//    std::cout << "32-aligned: " << is_aligned(w.data(), 32) <<std::endl;

    while (state.KeepRunning()) {
        v += alpha * w;
    }
    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_Vector_Eigen_AXPY)->Range(min_size, max_size)->Complexity(benchmark::oN);
#endif // ENABLE_EIGEN

static void BM_Vector_std_AXPY(benchmark::State& state) {
    int size = state.range(0);
    double alpha = 8.;
    std::vector<double> v(size), w(size);
//    std::cout << "32-aligned: " << is_aligned(w.data(), 32) <<std::endl;

    while (state.KeepRunning()) {
        for(int i = 0; i < size; ++i) {
            v[i] += alpha * w[i];
        }
    }
    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_Vector_std_AXPY)->Range(min_size, max_size)->Complexity(benchmark::oN);

static void BM_Vector_petscxx_AXPBYPCZ(benchmark::State& state) {
    if(ctx == nullptr) ctx = std::make_unique<petscxx::base::Context>();

    int size = state.range(0);
    double alpha = 8., beta = 6., gamma = 5.;
    petscxx::vector::Vector x(size), y(size), z(size);

    while (state.KeepRunning()) {
        z = alpha * x + beta * y + gamma * z;
    }
    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_Vector_petscxx_AXPBYPCZ)->Range(min_size, max_size)->Complexity(benchmark::oN);

static void BM_Vector_PETSc_AXPBYPCZ(benchmark::State& state) {
    Vec x, y, z;
    VecCreate(MPI_COMM_WORLD, &x);
    VecCreate(MPI_COMM_WORLD, &y);
    VecCreate(MPI_COMM_WORLD, &z);
    VecSetFromOptions(x);
    VecSetFromOptions(y);
    VecSetFromOptions(z);
    VecSetSizes(x, PETSC_DECIDE, state.range(0));
    VecSetSizes(y, PETSC_DECIDE, state.range(0));
    VecSetSizes(z, PETSC_DECIDE, state.range(0));
    double alpha = 8., beta = 6., gamma = 5.;

    while (state.KeepRunning()) {
        VecAXPBYPCZ(z, alpha, beta, gamma, x, y);
    }
    state.SetComplexityN(state.range(0));

    VecDestroy(&x);
    VecDestroy(&y);
    VecDestroy(&z);
}
BENCHMARK(BM_Vector_PETSc_AXPBYPCZ)->Range(min_size, max_size)->Complexity(benchmark::oN);

#if ENABLE_EIGEN
static void BM_Vector_Eigen_AXPBYPCZ(benchmark::State& state) {
    int size = state.range(0);
    double alpha = 8., beta = 6., gamma = 5.;
    Eigen::VectorXd x(size), y(size), z(size);

    while (state.KeepRunning()) {
        z = alpha * x + beta * y + gamma * z;
    }
    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_Vector_Eigen_AXPBYPCZ)->Range(min_size, max_size)->Complexity(benchmark::oN);
#endif // ENABLE_EIGEN

static void BM_Vector_std_AXPBYPCZ(benchmark::State& state) {
    int size = state.range(0);
    double alpha = 8., beta = 6., gamma = 5.;
    std::vector<double> x(size), y(size), z(size);

    double *const __attribute__((aligned(16))) x_ = x.data(),
            *const __attribute__((aligned(16))) y_ = y.data(),
            *const __attribute__((aligned(16))) z_ = z.data();

    while (state.KeepRunning()) {
        for(int i = 0; i < size; ++i) {
            z_[i] = alpha * x_[i] + beta * y_[i] + gamma * z_[i];
        }
    }
    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_Vector_std_AXPBYPCZ)->Range(min_size, max_size)->Complexity(benchmark::oN);

static void BM_Vector_petscxx_MAXPY(benchmark::State& state) {
    if(ctx == nullptr) ctx = std::make_unique<petscxx::base::Context>();

    int size = state.range(0);
    const int N = 9;
    double alpha[] = {1.,2.,3.,4.,5.,6.,7.,3.,4.,5.,6.,7.};
    petscxx::vector::Vector v[N] = {size,size, size, size, size, size, size, size, size};

    while (state.KeepRunning()) {
        v[0] += alpha[1] * v[1] + alpha[2] * v[2] + alpha[3] * v[3]
                + alpha[4] * v[4] + alpha[5] * v[5] + alpha[6] * v[6] + alpha[7] * v[7];
    }
    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_Vector_petscxx_MAXPY)->Range(min_size, max_size)->Complexity(benchmark::oN);

static void BM_Vector_PETSc_MAXPY(benchmark::State& state) {
    const int N = 8;
    Vec v[N];
    for(int i = 0; i < N; ++i) {
        VecCreate(MPI_COMM_WORLD, &(v[i]));
        VecSetFromOptions((v[i]));
        VecSetSizes(v[i], PETSC_DECIDE, state.range(0));
    }
    double alpha[] = {1.,2.,3.,4.,5.,6.,7.,8.,9.};

    while (state.KeepRunning()) {
        VecMAXPY(v[0], N-1, alpha, v+1);
    }
    state.SetComplexityN(state.range(0));

    for(int i = 0; i < N; ++i) {
        VecDestroy(v+i);
    }
}
BENCHMARK(BM_Vector_PETSc_MAXPY)->Range(8, max_size)->Complexity(benchmark::oN);

static void BM_Vector_petscxx_MultiBench(benchmark::State& state) {
    if(ctx == nullptr) ctx = std::make_unique<petscxx::base::Context>();

    int size = state.range(0);
    const int N = 9;
    double alpha[] = {1.,2.,3.,4.,5.,6.,7.,3.,4.,5.,6.,7.};
    petscxx::vector::Vector v[N] = {size,size, size, size, size, size, size, size, size};

    while (state.KeepRunning()) {
        (v[0] | v[1] | v[2] | v[3]) = (
              (alpha[1] * v[1] + alpha[2] * v[2]) |
              (alpha[3] * v[3] + alpha[4] * v[4] + alpha[5] * v[5]) |
              (v[6] * v[7] + v[4] * v[5]) |
              (v[1] + alpha[2] * v[2])
              );
    }
    state.SetComplexityN(state.range(0));
}
BENCHMARK(BM_Vector_petscxx_MultiBench)->Range(min_size, max_size)->Complexity(benchmark::oN);

static void BM_Vector_MultiBench(benchmark::State& state) {
    const int N = 8;
    Vec v[N];
    for(int i = 0; i < N; ++i) {
        VecCreate(MPI_COMM_WORLD, &(v[i]));
        VecSetFromOptions((v[i]));
        VecSetSizes(v[i], PETSC_DECIDE, state.range(0));
    }
    double alpha[] = {1.,2.,3.,4.,5.,6.,7.,8.,9.};

    while (state.KeepRunning()) {
        VecMAXPY(v[0], 2, alpha, v+1);
        VecMAXPY(v[1], 3, alpha+3, v+3);
        VecPointwiseMult(v[2], v[6], v[7]);
        VecPointwiseMult(v[4], v[4], v[5]);
        VecAXPY(v[2], 1, v[4]);
        double arr[] = {1, alpha[2]};
        VecMAXPY(v[3], 2, arr, v+1);
    }
    state.SetComplexityN(state.range(0));

    for(int i = 0; i < N; ++i) {
        VecDestroy(v+i);
    }
}
BENCHMARK(BM_Vector_MultiBench)->Range(8, max_size)->Complexity(benchmark::oN);

BENCHMARK_MAIN()//;
